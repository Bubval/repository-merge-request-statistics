# GitLab Merge Request Analyzer

This project fetches and analyzes merge requests from a specified GitLab project, providing insights into the number of comments, lines of code added, and lines of code removed for each user.

## Getting Started

### Prerequisites

Before you begin, ensure you have the following:
- Node.js installed on your machine.
- A GitLab account and access to a GitLab project.

### Setup

1. **Generate a GitLab Access Token**:
    - Go to your GitLab profile by clicking on your profile picture.
    - Navigate to `Preferences`.
    - In the side menu, select `Access Tokens`.
    - Create a new token with **all permissions** for full API access. This token is not project-specific but profile-level.
    - **Important**: Copy the token immediately as it cannot be retrieved after the page is refreshed.

2. **Find Your GitLab Project ID**:
    - Open your GitLab project repository.
    - The Project ID is displayed right under the project name. Note this ID down.

3. **Configure Environment Variables**:
    - In the root of this project, create a `.env` file.
    - Add your GitLab access token and project ID to the file:
      ```
      GITLAB_ACCESS_TOKEN="your_access_token_here"
      GITLAB_PROJECT_ID="your_project_id_here"
      ```
    - Replace `your_access_token_here` and `your_project_id_here` with the actual values.

4. **Install Dependencies**:
    - Run `npm install` in the project directory to install required packages.

### Running the Application

- Execute `node index.js` to start the application.
- The script will fetch and process merge requests and display the analysis in the console.

## Notes

- The `.env` file contains sensitive information. Do not share it or commit it to version control.
- Ensure you have the necessary permissions in the GitLab project to access merge requests and other data.
