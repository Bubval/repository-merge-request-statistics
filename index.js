import fetch from 'node-fetch';
import dotenv from 'dotenv';

dotenv.config();

const accessToken = process.env.GITLAB_ACCESS_TOKEN; 
const projectId = process.env.GITLAB_PROJECT_ID;
const createdAfter = process.env.GITLAB_CREATED_AFTER;

async function fetchAllMergeRequests(projectId, createdAfter) {
    const headers = { 'Authorization': `Bearer ${accessToken}` };
    let allMergeRequests = [];
    let page = 1;
    let hasMorePages = true;

    while (hasMorePages) {
        const url = new URL(`https://gitlab.com/api/v4/projects/${projectId}/merge_requests`);
        url.searchParams.append('created_after', createdAfter);
        url.searchParams.append('per_page', '100');
        url.searchParams.append('page', page.toString());

        const response = await fetch(url.toString(), { headers });
        const mergeRequests = await response.json();
        allMergeRequests = allMergeRequests.concat(mergeRequests);

        const linkHeader = response.headers.get('link');
        if (linkHeader && linkHeader.includes('rel="next"')) {
            page++;
        } else {
            hasMorePages = false;
        }
    }

    return allMergeRequests;
}


async function fetchDiscussions(projectId, mergeRequestId) {
  const headers = { 'Authorization': `Bearer ${accessToken}` };
  const response = await fetch(`https://gitlab.com/api/v4/projects/${projectId}/merge_requests/${mergeRequestId}/discussions`, { headers });
  return response.json();
}

async function fetchMergeRequestChanges(projectId, mergeRequestId) {
    const headers = { 'Authorization': `Bearer ${accessToken}` };
    const response = await fetch(`https://gitlab.com/api/v4/projects/${projectId}/merge_requests/${mergeRequestId}/changes`, { headers });
    return response.json();
}

function countLinesFromDiff(diffString) {
    const lines = diffString.split('\n');
    let linesAdded = 0;
    let linesRemoved = 0;

    lines.forEach(line => {
        if (line.startsWith('+') && !line.startsWith('+++')) {
            linesAdded++;
        } else if (line.startsWith('-') && !line.startsWith('---')) {
            linesRemoved++;
        }
    });

    return { linesAdded, linesRemoved };
}

const formatDate = (date) => new Date(date).toISOString();

function mean(numbersArr)
{
    const length = numbersArr.length;
    if (length === 0) return 0;
    let sum = 0;
    for (let i = 0; i < length; i++)
        sum += numbersArr[i];
 
    return sum / length;
}

function percentile(numbersArr, percentile) {
    const length = numbersArr.length;
    if (length === 0) {
        return null;
    }

    numbersArr.sort((a, b) => a - b);
    const index = (percentile / 100) * (length - 1);

    if (Number.isInteger(index)) {
        return numbersArr[index];
    } else {
        const lower = Math.floor(index);
        const upper = Math.ceil(index);
        return numbersArr[lower] + (numbersArr[upper] - numbersArr[lower]) * (index - lower);
    }
}

function standardDeviation(numbersArr) {
    const length = numbersArr.length;
    if (length === 0 || length === 1) {
        return null;
    }

    const mean = numbersArr.reduce((acc, val) => acc + val, 0) / length;
    const variance = numbersArr.reduce((acc, val) => acc + (val - mean) ** 2, 0) / length;
    return Math.sqrt(variance);
}

function variance(numbersArr) {
    const length = numbersArr.length;
    if (length === 0 || length === 1) {
        return null;
    }

    const mean = numbersArr.reduce((acc, val) => acc + val, 0) / length;
    return numbersArr.reduce((acc, val) => acc + (val - mean) ** 2, 0) / length;
}

function range(numbersArr) {
    if (numbersArr.length === 0) {
        return null;
    }

    return Math.max(...numbersArr) - Math.min(...numbersArr);
}

function skewness(numbersArr) {
    const length = numbersArr.length;
    if (length === 0 || length === 1) {
        return null;
    }

    const mean = numbersArr.reduce((acc, val) => acc + val, 0) / length;
    const thirdMoment = numbersArr.reduce((acc, val) => acc + (val - mean) ** 3, 0) / length;
    const stdDev = Math.sqrt(numbersArr.reduce((acc, val) => acc + (val - mean) ** 2, 0) / length);
    return thirdMoment / Math.pow(stdDev, 3);
}




async function main() {
    console.log(`\x1b[35m${formatDate(new Date())} 🚀 FETCHING MERGE REQUESTS\x1b[0m`);
    const mergeRequests = await fetchAllMergeRequests(projectId, createdAfter);

    const statsForUser = new Map();

    for (const mr of mergeRequests) {
        const mrAuthor = mr.author.username;
        if (!statsForUser.has(mrAuthor)) {
            statsForUser.set(mrAuthor, { mrCount: 0, commentsPerMR: [], linesAddedPerMR: [], linesRemovedPerMR: [] });
        } 

        console.log(`\x1b[36mMR Title: ${mr.title}\x1b[0m`);
        console.log(`\x1b[32mAuthor: ${mr.author.username}\x1b[0m`);
        console.log(`\x1b[33mCreated: ${formatDate(mr.created_at)} | Updated: ${formatDate(mr.updated_at)}\x1b[0m`);
        console.log('---------------------------------------------------');

        const discussions = await fetchDiscussions(projectId, mr.iid);
        let commentsTotal = 0;
        for (const discussion of discussions) {
            for (const note of discussion.notes) {
                if (note.author && note.author.username) {
                    const commentAuthor = note.author.username;
                    // Get only comments from other users
                    // (i.e. ignore comments from the MR author)
                    if (commentAuthor !== mrAuthor) {
                        commentsTotal += 1;
                    }
                }
            }
        }

        const changes = await fetchMergeRequestChanges(projectId, mr.iid);
        let linesAddedTotal = 0;
        let linesRemovedTotal = 0
        changes.changes.forEach(change => {
            const {linesAdded, linesRemoved} = countLinesFromDiff(change.diff);
            linesAddedTotal += linesAdded;
            linesRemovedTotal += linesRemoved;
        });

        const currentUserStats = statsForUser.get(mrAuthor);
        currentUserStats.mrCount = currentUserStats.mrCount + 1;
        currentUserStats.commentsPerMR.push(commentsTotal);
        currentUserStats.linesAddedPerMR.push(linesAddedTotal);
        currentUserStats.linesRemovedPerMR.push(linesRemovedTotal);
        statsForUser.set(mrAuthor, currentUserStats);
    }

    console.log('\x1b[34m========== Stats for Users ==========\x1b[0m');
    statsForUser.forEach((value, key) => {
        const { mrCount, commentsPerMR, linesAddedPerMR, linesRemovedPerMR } = value;

        const linesAdded = linesAddedPerMR.reduce((a, b) => a + b, 0);
        const linesRemoved = linesRemovedPerMR.reduce((a, b) => a + b, 0);
        const comments = commentsPerMR.reduce((a, b) => a + b, 0);

        let codePerComment = null;
        if (comments > 0) codePerComment = linesAdded  / comments;
        
        const meanComments = mean(commentsPerMR);
        const percentile25Comments = percentile(commentsPerMR, 25);
        const percentile50Comments = percentile(commentsPerMR, 50);
        const percentile75Comments = percentile(commentsPerMR, 75);
        const standardDevicationComments = standardDeviation(commentsPerMR);
        const varianceComments = variance(commentsPerMR);
        const rangeComments = range(commentsPerMR);
        const skewnessComments = skewness(commentsPerMR);

        const meanCode = mean(linesAddedPerMR);
        const percentile25Code = percentile(linesAddedPerMR, 25);
        const percentile50Code = percentile(linesAddedPerMR, 50);
        const percentile75Code = percentile(linesAddedPerMR, 75);
        const standardDevicationCode = standardDeviation(linesAddedPerMR);
        const varianceCode = variance(linesAddedPerMR);
        const rangeCode = range(linesAddedPerMR);
        const skewnessCode = skewness(linesAddedPerMR);


        console.log(`\x1b[33mUser: \x1b[32m${key}\x1b[0m`);
        console.log(`\x1b[36mTotal Merge Requests: \x1b[35m${mrCount}\x1b[0m`);
        console.log(`\x1b[36mLines of Code Added: \x1b[35m${linesAdded}`);
        console.log(`\x1b[36mLines of Code Removed: \x1b[35m${linesRemoved}\x1b[0m`);
        console.log(`\x1b[36mComments: \x1b[35m${comments}`);
        if (codePerComment !== null) {
            console.log('')
            console.log(`\x1b[36mLines of Code per Comment: \x1b[35m${codePerComment.toFixed(2)}\x1b[0m`);
        }
        console.log('')
        console.log(`Comments for MR:`);
        typeof mean === 'number' && console.log(`\x1b[36mAverage: \x1b[35m${meanComments.toFixed(2)}\x1b[0m`);
        percentile25Comments !== null && console.log(`\x1b[36m25th percentile: \x1b[35m${percentile25Comments.toFixed(2)}\x1b[0m`)
        percentile50Comments !== null&& console.log(`\x1b[36mMedian (50th percentile): \x1b[35m${percentile50Comments.toFixed(2)}\x1b[0m`)
        percentile75Comments !== null && console.log(`\x1b[36m75th percentile: \x1b[35m${percentile75Comments.toFixed(2)}\x1b[0m`)
        standardDevicationComments !== null && console.log(`\x1b[36mStandard Deviation: \x1b[35m${standardDevicationComments.toFixed(2)}\x1b[0m`)
        varianceComments !== null && console.log(`\x1b[36mVariance: \x1b[35m${varianceComments.toFixed(2)}\x1b[0m`)
        rangeComments !== null && console.log(`\x1b[36mRange: \x1b[35m${rangeComments.toFixed(2)}\x1b[0m`)
        skewnessComments !== null && console.log(`\x1b[36mSkewness: \x1b[35m${skewnessComments.toFixed(2)}\x1b[0m`)
        console.log('')
        console.log(`Lines of Code Added for MR:`)
        typeof meanCode === 'number' && console.log(`\x1b[36mAverage: \x1b[35m${meanCode.toFixed(2)}\x1b[0m`);
        percentile25Code !== null && console.log(`\x1b[36m25th percentile: \x1b[35m${percentile25Code.toFixed(2)}\x1b[0m`)
        percentile50Code !== null && console.log(`\x1b[36mMedian (50th percentile): \x1b[35m${percentile50Code.toFixed(2)}\x1b[0m`)
        percentile75Code !== null && console.log(`\x1b[36m75th percentile: \x1b[35m${percentile75Code.toFixed(2)}\x1b[0m`)
        standardDevicationCode !== null && console.log(`\x1b[36mStandard Deviation: \x1b[35m${standardDevicationCode.toFixed(2)}\x1b[0m`)
        varianceCode !== null && console.log(`\x1b[36mVariance: \x1b[35m${varianceCode.toFixed(2)}\x1b[0m`)
        rangeCode !== null && console.log(`\x1b[36mRange: \x1b[35m${rangeCode.toFixed(2)}\x1b[0m`)
        skewnessCode !== null && console.log(`\x1b[36mSkewness: \x1b[35m${skewnessCode.toFixed(2)}\x1b[0m`)
        console.log('\x1b[34m------------------------------------------\x1b[0m');
    });
    console.log('\x1b[34m====================================\x1b[0m');
}

main().catch(console.error);